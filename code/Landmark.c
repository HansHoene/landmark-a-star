/*
    File: "Landmark.c"
    Author: Hans-Edward Hoene
*/

#include "Landmark.h"
#include "types.h"
#include "Graph.h"
#include "SSSP.h"

#include <stdio.h>
#include <stdlib.h>

// return difference, or zero
#define SUB(a, b)   (((a) > (b)) ? ((a) - (b)) : 0)

/* -------------------- H e l p e r    F u n c t i o n s -------------------- */

/*
    Purpose: Get distance to landmark
    Arguments:
        [in ] Ldmk  *lm      - landmark data structure
        [in ] Index vertex   - index of vertex
        [in ] Index landmark - index of landmark
    Return:
        Shortest distance from vertex to landmark
*/
static Dist DistToLdmk(Ldmk *lm, Index vertex, Index landmark);

/*
    Purpose: Get distance from landmark
    Arguments:
        [in ] Ldmk  *lm      - landmark data structure
        [in ] Index landmark - index of landmark
        [in ] Index vertex   - index of vertex
    Return:
        Shortest distance to vertex from landmark
*/
static Dist DistFromLdmk(Ldmk *lm, Index landmark, Index vertex);

/*
    Purpose: Set distance to landmark
    Arguments:
        [in ] Ldmk  *lm      - landmark data structure
        [in ] Index vertex   - index of vertex
        [in ] Index landmark - index of landmark
*/
static void SetDistToLdmk(Ldmk *lm, Index vertex, Index landmark, Dist dist);

/*
    Purpose: Set distance from landmark
    Arguments:
        [in ] Ldmk  *lm      - landmark data structure
        [in ] Index landmark - index of landmark
        [in ] Index vertex   - index of vertex
*/
static void SetDistFromLdmk(Ldmk *lm, Index landmark, Index vertex, Dist dist);

/* -------------------- P u b l i c    F u n c t i o n s -------------------- */
int LoadLdmk(Ldmk *lm, const char fname[]) {
    FILE            *file;      // file pointer
    unsigned int    tblSize;    // size of table
    size_t          flag;       // error flag
    
    // open file
    file = fopen(fname, "rb");
    if (file == NULL) {
        return FILE_ERROR;
    }
    
    // read # of vertices and # of landmarks
    flag = 1 - fread(&lm->numVert, sizeof(unsigned int), 1, file);
    if (flag) {
        fclose(file);
        return FILE_ERROR;
    }
    flag = 1 - fread(&lm->numLdmk, sizeof(unsigned int), 1, file);
    if (flag) {
        fclose(file);
        return FILE_ERROR;
    }
    
    // allocate data
    tblSize = lm->numVert * lm->numLdmk;
    lm->distFrom = (Dist *)malloc(sizeof(Dist) * tblSize * 2);
    if (lm->distFrom == NULL) {
        fclose(file);
        return ALLOC_ERROR;
    }
    lm->distTo = &lm->distFrom[tblSize];
    
    // read tabular data
    flag = tblSize - fread(lm->distFrom, sizeof(Dist), tblSize, file);
    if (flag) {
        free(lm->distFrom);
        fclose(file);
        return FILE_ERROR;
    }
    flag = tblSize - fread(lm->distTo, sizeof(Dist), tblSize, file);
    fclose(file);
    if (flag) {
        free(lm->distFrom);
        return FILE_ERROR;
    }
    
    return NO_ERROR;
}

int StoreLdmk(Ldmk *lm, const char fname[]) {
    FILE            *file;      // file pointer
    unsigned int    tblSize;    // size of table
    size_t          flag;       // error flag
    
    // open file
    file = fopen(fname, "wb");
    if (file == NULL) {
        return FILE_ERROR;
    }
    
    // write # of vertices and # of landmarks
    flag = 1 - fwrite(&lm->numVert, sizeof(unsigned int), 1, file);
    if (flag) {
        fclose(file);
        return FILE_ERROR;
    }
    flag = 1 - fwrite(&lm->numLdmk, sizeof(unsigned int), 1, file);
    if (flag) {
        fclose(file);
        return FILE_ERROR;
    }
    
    // write tabular data
    tblSize = lm->numVert * lm->numLdmk;
    flag = tblSize - fwrite(lm->distFrom, sizeof(Dist), tblSize, file);
    if (flag) {
        fclose(file);
        return FILE_ERROR;
    }
    flag = tblSize - fwrite(lm->distTo, sizeof(Dist), tblSize, file);
    fclose(file);
    if (flag) {
        return FILE_ERROR;
    }
    
    return NO_ERROR;
}

void TriangleInequality(Ldmk *lm, Index start, Index end,
                        Dist *min, Dist *max) {
    Dist            lower;  // lower bound
    Dist            upper;  // upper bound
    Dist            temp;  // temporary value
    unsigned int    i;
    
    // set starting bounds
    lower = 0;
    upper = INFINITY;
    
    // use landmarks to narrow bounds
    for (i = 0; i < lm->numLdmk; i++) {
        /* Triangle Inequalities:
            d(a, b) <= d(a, l) + d(l, b)
            d(a, l) <= d(a, b) + d(b, l)
            d(l, b) <= d(l, a) + d(a, b)
        */
        
        // calulcate upper bound with this landmark
        temp =
            DistToLdmk(lm, start, i) +
            DistFromLdmk(lm, i, end);
        
        // is this smallest upper bound?
        if (temp < upper) {
            upper = temp;
        }
        
        // calculate first lower bound with this landmark
        temp = SUB(
            DistToLdmk(lm, start, i),
            DistToLdmk(lm, end, i)
        );
        
        // is the largest lower bound?
        if (temp > lower) {
            lower = temp;
        }
        
        // calculate second lower bound with this landmark
        temp = SUB(
            DistFromLdmk(lm, i, end),
            DistFromLdmk(lm, i, start)
        );
        
        // is the largest lower bound?
        if (temp > lower) {
            lower = temp;
        }  
    }
    
    *min = lower;
    *max = upper;
}
                        
int CreateLdmk(Ldmk *lm, Graph *graph, Index landmarks[], Index size) {
    unsigned int i, j;
    Stats stats;
    int flag;
    
    // allocate landmarks
    lm->numVert = graph->numVertices;
    lm->numLdmk = size;
    lm->distFrom = (Dist *)malloc(sizeof(Dist) * lm->numVert * lm->numLdmk * 2);
    if (lm->distFrom == NULL) {
        return ALLOC_ERROR;
    }
    lm->distTo = &lm->distFrom[lm->numVert * lm->numLdmk];
    
    // run SSSP from every landmark
    for (i = 0; i < size; i++) {
        flag = Dijkstra(graph, landmarks[i], graph->numVertices, &stats);
        if (flag != NO_ERROR) {
            free(lm->distFrom);
            free(lm->distTo);
            return flag;
        }
        
        for (j = 0; j < graph->numVertices; j++) {
            SetDistFromLdmk(lm, i, j, stats.distances[j]);
        }
        free(stats.parents);
        free(stats.distances);
    }
    
    // run single destination shortest-path
    // reverse edges and do SSSP to get shortest paths to landmarks
    flag = ReverseEdges(graph);
    if (flag != NO_ERROR) {
        return flag;
    }
    for (i = 0; i < size; i++) {
        flag = Dijkstra(graph, landmarks[i], graph->numVertices, &stats);
        if (flag != NO_ERROR) {
            free(lm->distFrom);
            free(lm->distTo);
            return flag;
        }
        
        for (j = 0; j < graph->numVertices; j++) {
            SetDistToLdmk(lm, j, i, stats.distances[j]);
        }
        free(stats.parents);
        free(stats.distances);
    }
    flag = ReverseEdges(graph);
    if (flag != NO_ERROR) {
        return flag;
    }
    
    return NO_ERROR;
}

void PrintLdmkData(Ldmk *lm) {
    unsigned int i, j;
    
    printf("* Distances From Landmarks *\n");
    printf("            |  ");
    for (i = 0; i < lm->numLdmk; i++) {
        printf("%10u  ", i);
    }
    printf("\n");
    printf("------------|--");
    for (i = 0; i < lm->numLdmk; i++) {
        printf("------------");
    }
    printf("\n");
    
    for (i = 0; i < lm->numVert; i++) {
        printf("%10u  |  ", i);
        for (j = 0; j < lm->numLdmk; j++) {
            printf("%10u  ", DistFromLdmk(lm, j, i));
        }
        printf("\n");
    }
    
    printf("\n* Distances To Landmarks *\n");
    printf("            |  ");
    for (i = 0; i < lm->numLdmk; i++) {
        printf("%10u  ", i);
    }
    printf("\n");
    printf("------------|--");
    for (i = 0; i < lm->numLdmk; i++) {
        printf("------------");
    }
    printf("\n");
    
    for (i = 0; i < lm->numVert; i++) {
        printf("%10u  |  ", i);
        for (j = 0; j < lm->numLdmk; j++) {
            printf("%10u  ", DistToLdmk(lm, i, j));
        }
        printf("\n");
    }
}

void FreeLdmk(Ldmk *lm) {
    free(lm->distFrom);
    // distTo is stored at the end of the same array; only free once
}

/* ------------------- H e l p e r    F u n c    D e f s -------------------- */

static Dist DistToLdmk(Ldmk *lm, Index vertex, Index landmark) {
    return lm->distTo[
        (vertex * lm->numLdmk) + landmark
    ];
}

static Dist DistFromLdmk(Ldmk *lm, Index landmark, Index vertex) {
    return lm->distFrom[
        (vertex * lm->numLdmk) + landmark
    ];
}

static void SetDistToLdmk(Ldmk *lm, Index vertex, Index landmark, Dist dist) {
    lm->distTo[
        (vertex * lm->numLdmk) + landmark
    ] = dist;
}

static void SetDistFromLdmk(Ldmk *lm, Index landmark, Index vertex, Dist dist) {
    lm->distFrom[
        (vertex * lm->numLdmk) + landmark
    ] = dist;
}