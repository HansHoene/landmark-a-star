/*
    File: "RunPre.c"
    Author: Hans-Edward Hoene
*/

#include "Graph.h"
#include "Landmark.h"

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define NUM_ARGS        (3)
#define ARG_GRAPH       (1)
#define ARG_LDMK        (2)
#define ARG_NUMLDMK     (3)

int main(int argc, char *argv[]) {
    Index *landmarks;
    unsigned int numLdmks;
    Graph graph;
    int flag;
    Ldmk land;
    
    if (argc != (NUM_ARGS + 1)) {
        printf("error\n");
        return 0;
    }
    
    flag = ReadGraphFromFile(&graph, argv[ARG_GRAPH]);
    if (flag != NO_ERROR) {
        printf("error\n");
        return 0;
    }
    
    flag = 1 - sscanf(argv[ARG_NUMLDMK], "%u", &numLdmks);
    if (flag) {
        DestroyGraph(&graph);
        printf("error\n");
        return 0;
    }
    landmarks = (Index *)malloc(sizeof(Index) * numLdmks);
    if (landmarks == NULL) {
        DestroyGraph(&graph);
        printf("error\n");
        return 0;
    }
    
    srand(time(NULL));
    printf("Landmarks = { ");
    for (flag = 0; flag < numLdmks; flag++) {
        landmarks[flag] = rand() % graph.numVertices;
        printf("%u%s ", landmarks[flag], (flag < (numLdmks - 1)) ? "," : "");
    }
    printf("}\n");
    
    flag = CreateLdmk(&land, &graph, landmarks, numLdmks);
    free(landmarks);
    DestroyGraph(&graph);
    if (flag) {
        printf("error\n");
        return 0;
    }
    
    flag = StoreLdmk(&land, argv[ARG_LDMK]);
    PrintLdmkData(&land);
    FreeLdmk(&land);
    if (flag) {
        printf("error\n");
    }
    
    return 0;
}
