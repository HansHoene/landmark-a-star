/*
    File: "SSSP.h"
    Author: Hans-Edward Hoene
*/

#ifndef _SSSP_H_
#define _SSSP_H_

#include "Graph.h"
#include "stats.h"
#include "Landmark.h"

/*
    Purpose: Run Dijkstra's Algorithm
    Arguments:
        Graph *graph - the graph
        Index start  - starting vertex
        Index target - end vertex
        Stats *stats - statistics
    Return:
        Error code
*/
int Dijkstra(Graph *graph, Index start, Index target, Stats *stats);

/*
    Purpose: Run Landmark A* Algorithm
    Arguments:
        Graph *graph - the graph
        Ldmk  *lm    - landmarks
        Index start  - starting vertex
        Index target - end vertex
        Stats *stats - statistics
    Return:
        Error code
*/
int LandmarkAStar(Graph *graph, Ldmk *lm, Index start, Index target, Stats *stats);

#endif
