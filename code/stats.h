/*
    File: "stats.h"
    Author: Hans-Edward Hoene
*/

#ifndef _STATS_H_
#define _STATS_H_

#include "Graph.h"

typedef struct {
    unsigned int vertexCounter;
    unsigned int edgeCounter;
    Dist *distances;
    Index *parents;
} Stats;

#endif
