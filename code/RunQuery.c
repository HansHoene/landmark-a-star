/*
    File: "RunQuery.c"
    Author: Hans-Edward Hoene
*/

#include "Graph.h"
#include "Landmark.h"
#include "stats.h"
#include "SSSP.h"

#include <stdio.h>
#include <stdlib.h>

#define NUM_ARGS        (4)
#define ARG_GRAPH       (1)
#define ARG_LDMK        (2)
#define ARG_S           (3)
#define ARG_T           (4)

int main(int argc, char *argv[]) {
    Graph graph;
    int flag;
    Ldmk land;
    Index s, t;
    Stats stats;
    
    if (argc != (NUM_ARGS + 1)) {
        printf("error\n");
        return 0;
    }
    
    flag = ReadGraphFromFile(&graph, argv[ARG_GRAPH]);
    if (flag != NO_ERROR) {
        printf("error\n");
        return 0;
    }
    
    flag = LoadLdmk(&land, argv[ARG_LDMK]);
    if (flag) {
        DestroyGraph(&graph);
        printf("error\n");
        return 0;
    }
    
    flag = 1 - sscanf(argv[ARG_S], "%u", &s);
    if (flag) {
        DestroyGraph(&graph);
        FreeLdmk(&land);
        printf("error\n");
        return 0;
    }
    flag = 1 - sscanf(argv[ARG_T], "%u", &t);
    if (flag) {
        DestroyGraph(&graph);
        FreeLdmk(&land);
        printf("error\n");
        return 0;
    }
    
    // landmark a*
    flag = LandmarkAStar(&graph, &land, s, t, &stats);
    if (flag != NO_ERROR) {
        DestroyGraph(&graph);
        FreeLdmk(&land);
        printf("error\n");
        return 0;
    }
    printf("Landmark A* Results\n");
    if (stats.distances[t] == INFINITY) {
        printf("  No path found.\n");
    } else {
        printf("  Shortest Path = %u\n", stats.distances[t]);
        printf("  Traceback = { ");
        for (flag = t; flag != s; flag = stats.parents[flag]) {
            printf("%u, ", flag);
        }
        printf("%u }\n", s);
    }
    printf("  # of Vertices Traversed = %u\n", stats.vertexCounter);
    printf("  # of Edges Expanded = %u\n\n", stats.edgeCounter);
    free(stats.parents);
    free(stats.distances);
    
    // Dijkstra
    flag = Dijkstra(&graph, s, t, &stats);
    if (flag != NO_ERROR) {
        DestroyGraph(&graph);
        FreeLdmk(&land);
        printf("error\n");
        return 0;
    }
    printf("Dijkstra's Algorithm Results\n");
    if (stats.distances[t] == INFINITY) {
        printf("  No path found.\n");
    } else {
        printf("  Shortest Path = %u\n", stats.distances[t]);
        printf("  Traceback = { ");
        for (flag = t; flag != s; flag = stats.parents[flag]) {
            printf("%u, ", flag);
        }
        printf("%u }\n", s);
    }
    printf("  # of Vertices Traversed = %u\n", stats.vertexCounter);
    printf("  # of Edges Expanded = %u\n\n", stats.edgeCounter);
    free(stats.parents);
    free(stats.distances);
    
    // free
    DestroyGraph(&graph);
    FreeLdmk(&land);
    
    return 0;
}
