/*
    File: "SSSP.c"
    Author: Hans-Edward Hoene
*/

#include "SSSP.h"
#include "Graph.h"
#include "types.h"
#include "Landmark.h"

#include <stdlib.h>

#define SETTLED (0)
#define QUEUED (1)
#define UNVISITED (2)

int Dijkstra(Graph *graph, Index start, Index target, Stats *stats) {
    unsigned int i;
    unsigned int numNeighbours;
    Edge edge;
    int *states;
    Dist lowest;
    
    // allocate
    stats->edgeCounter = 0;
    stats->vertexCounter = 0;
    stats->distances = (Dist *)malloc(sizeof(Dist) * graph->numVertices);
    if (stats->distances == NULL) {
        return ALLOC_ERROR;
    }
    stats->parents = (Index *)malloc(sizeof(Index) * graph->numVertices);
    if (stats->parents == NULL) {
        free(stats->distances);
        return ALLOC_ERROR;
    }
    states = (int *)malloc(sizeof(int) * graph->numVertices);
    if (states == NULL) {
        free(stats->distances);
        free(stats->parents);
        return ALLOC_ERROR;
    }
    
    // initialise
    for (i = 0; i < graph->numVertices; i++) {
        stats->distances[i] = INFINITY;
        stats->parents[i] = graph->numVertices;
        states[i] = UNVISITED;
    }
    stats->parents[start] = start;
    stats->distances[start] = 0;
    
    // run Algorithm
    while ((start != target) && (start < graph->numVertices)) {
        states[start] = SETTLED;
        ++stats->vertexCounter;
        
        // expand and relax all neighbours
        numNeighbours = GetNumNeighbours(graph, start);
        stats->edgeCounter += numNeighbours;
        for (i = 0; i < numNeighbours; i++) {
            // if shorter path found, update path
            edge = GetEdge(graph, start, i);
            if ((stats->distances[start] + edge.weight) <
                            stats->distances[edge.dest]) {
                stats->distances[edge.dest] =
                    stats->distances[start] + edge.weight;
                stats->parents[edge.dest] = start;
                states[edge.dest] = QUEUED;
            }
        }
        
        // choose next vertex (vertex with smallest distance)
        start = graph->numVertices;
        lowest = INFINITY;
        for (i = 0; i < graph->numVertices; i++) {
            if (states[i] == QUEUED) {
                if (stats->distances[i] < lowest) {
                    start = i;
                    lowest = stats->distances[i];
                }
            }
        }
    }
    
    free(states);
    return NO_ERROR;
}

int LandmarkAStar(Graph *graph, Ldmk *lm, Index start, Index target, Stats *stats) {
    unsigned int i;
    unsigned int numNeighbours;
    Edge edge;
    int *states;
    Dist temp1, temp2;
    Dist lowest;
    
    // allocate
    stats->edgeCounter = 0;
    stats->vertexCounter = 0;
    stats->distances = (Dist *)malloc(sizeof(Dist) * graph->numVertices);
    if (stats->distances == NULL) {
        return ALLOC_ERROR;
    }
    stats->parents = (Index *)malloc(sizeof(Index) * graph->numVertices);
    if (stats->parents == NULL) {
        free(stats->distances);
        return ALLOC_ERROR;
    }
    states = (int *)malloc(sizeof(int) * graph->numVertices);
    if (states == NULL) {
        free(stats->distances);
        free(stats->parents);
        return ALLOC_ERROR;
    }
    
    // initialise
    for (i = 0; i < graph->numVertices; i++) {
        stats->distances[i] = INFINITY;
        stats->parents[i] = graph->numVertices;
        states[i] = UNVISITED;
    }
    stats->parents[start] = start;
    stats->distances[start] = 0;
    
    // run Algorithm
    while (start != target && start < graph->numVertices) {
        states[start] = SETTLED;
        ++stats->vertexCounter;
        
        // expand and relax all neighbours
        numNeighbours = GetNumNeighbours(graph, start);
        stats->edgeCounter += numNeighbours;
        for (i = 0; i < numNeighbours; i++) {
            // if shorter path found, update path
            edge = GetEdge(graph, start, i);
            if ((stats->distances[start] + edge.weight) <
                            stats->distances[edge.dest]) {
                stats->distances[edge.dest] =
                    stats->distances[start] + edge.weight;
                stats->parents[edge.dest] = start;
                states[edge.dest] = QUEUED;
            }
        }
        
        // choose next vertex (vertex with smallest distance)
        start = graph->numVertices;
        lowest = INFINITY;
        for (i = 0; i < graph->numVertices; i++) {
            if (states[i] == QUEUED) {
                TriangleInequality(lm, i, target, &temp1, &temp2);
                if ((stats->distances[i] + temp1) < lowest) {
                    start = i;
                    lowest = stats->distances[i] + temp1;
                }
            }
        }
    }
    
    free(states);
    return NO_ERROR;
}
