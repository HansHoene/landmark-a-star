/*
    File: "Landmark.h"
    Author: Hans-Edward Hoene
*/

#ifndef _LANDMARK_H_
#define _LANDMARK_H_

#include "types.h"
#include "Graph.h"

/*
    Landmark Data Structure
    
    This data structure stores landmarks in a tabular format.  Columns indices
    correspond to landmark indices.  Row indices correspond to vertex indices.
    The intersection of a row and column holds the shortest distance between a
    landmark and a vertex.  Two tables are needed.  One table holds distances
    from landmarks.  Another table holds distances to landmarks.
*/
typedef struct {
    Dist            *distTo;    // shortest distances to landmarks
    Dist            *distFrom;  // shortest distance from landmarks
    unsigned int    numVert;    // # of vertices
    unsigned int    numLdmk;    // # of landmarks
} Ldmk;

/*
    Purpose: Load landmark data from binary file
    Arguments:
        [out] Ldmk *lm           - landmark data structure
        [in ] const char fname[] - file name
    Return:
        Error code.
*/
int LoadLdmk(Ldmk *lm, const char fname[]);

/*
    Purpose: Store landmark data to binary file
    Arguments:
        [in ] Ldmk *lm           - landmark data structure
        [in ] const char fname[] - file name
    Return:
        Error code.
*/
int StoreLdmk(Ldmk *lm, const char fname[]);

/*
    Purpose: Get bounds defined by triangle inequality
    Arguments:
        [in ] Ldmk *lm       - landmark data structure
        [in ] Index start    - index of starting vertex
        [in ] Index end      - index of ending vertex
        [in ] Index landmark - index of landmark
        [out] Dist *min      - lower bound of triangle inequality
        [out] Dist *max      - upper bound of triangle inequality
*/
void TriangleInequality(Ldmk *lm, Index start, Index end,
                        Dist *min, Dist *max);
                        
/*
    Purpose: Preprocess a graph to get landmark data structure
    Arguments:
        [out] Ldmk  *lm         - landmark data structure
        [in ] Graph *graph      - input graph
        [in ] Index landmarks[] - indices of landmarks
        [in ] Index size        - size of landmarks array
    Return:
        Error code
*/
int CreateLdmk(Ldmk *lm, Graph *graph, Index landmarks[], Index size);

/*
    Purpose: Print Landmark Data
    Arguments:
        [in ] Ldmk  *lm
*/
void PrintLdmkData(Ldmk *lm);

/*
    Purpose: Free landmarks
    Arguments:
        [in ] Ldmk *lm - landmark data structure
*/
void FreeLdmk(Ldmk *lm);

#endif
