# ------------------------------- C o m p i l e --------------------------------

# compile Graph Library
cd Graph-Library
make
cd build
if [ ! -f ./hgi.exe ] || [ ! -f ./hgp.exe ] || [ ! -f ./libGraph.dll ]; then
    echo "*** Error: did not compile"
    exit /b 0
fi
cd ../..

# copy Graph Library into Landmark A* code directory
cp ./Graph-Library/libGraph.dll ./code
cp ./Graph-Library/hgi.exe ./code
cp ./Graph-Library/hgp.exe ./code

# compile code
cd code
make
if [ ! -f ./RunPre.exe ] || [ ! -f ./RunQuery.exe ]; then
    echo "*** Error: did not compile"
    exit /b 0
fi
cd ..

# -------------------------- T e s t    V e c t o r s --------------------------
dataSets=( facebookSmall facebookBig twitter )
s_arr=(1 5 30 55 100)
t_arr=(55 99 32 12 0 4)

cd code
for data in ${dataSets[@]}; do
    # parse raw data into binary CSR graph
    ./hgp.exe $data.bin $data.txt 0
    
    # print some basic information
    ./hgi.exe $data.bin | echo "1 2 3 4"
    
    # preprocess landmarks
    ./RunPre.exe $data.bin $data.ldmk.bin 20
    
    # run a few queries
    for s in ${s_arr[@]}; do
        for t in ${t_arr[@]}; do
            ./RunQuery.exe $data.bin $data.ldmk.bin $s $t
        done
    done
done
cd ..
